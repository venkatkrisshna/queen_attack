! ==================== !
! Programming Practice !
! ==================== !
! Venkata Krisshna

! Queen attack
! http://users.csc.calpoly.edu/~jdalbey/103/Projects/ProgrammingPractice.html#easy - Easy 3

program queen_attack
  implicit none
  integer,dimension(8,8) :: board
  integer,dimension(2,2) :: pos

  print*,'==================================='
  print*,'Programming Practice - Queen Attack'
  print*,'==================================='
  call init
  call solve

contains

  ! Accept board
  subroutine init
    implicit none
    integer :: i,j,count
    
    ! Read board
    open(21, file="board")
    read(21,*) board
    board = transpose(board)

    ! Determine queen positions
    count = 0
    main: do i=1,8
       do j=1,8
          if (board(i,j).eq.1) then
             count = count + 1
             pos(count,:) = [i,j]
             if (count.eq.2) exit main
          end if
       end do
    end do main

    ! Single queen
    if(count.eq.1)then
       print*,'Place two queens on the board!'
       stop
    end if
    
    return
  end subroutine init

  ! Determine if attack is possible
  subroutine solve
    implicit none

    if (pos(1,1).eq.pos(2,1)) then
       print*,'Queens are on the same row! ATTACK!'
    else if (pos(1,2).eq.pos(2,2)) then
       print*,'Queens are on the same column! ATTACK!'
    else if (abs(pos(2,1)-pos(1,1)).eq.abs(pos(2,2)-pos(1,2))) then
       print*,'Queens are on the same diagonal! ATTACK!'
    else
       print*,'Queens are safe!'
    end if

    return
  end subroutine solve

end program queen_attack
